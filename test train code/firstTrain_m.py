#import tensorflow , keras , matplotlib, numpy
import tensorflow as tf
import keras
import matplotlib.pyplot as plt
import numpy as np


# ( i allready did ) load data
fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images,test_labels) = fashion_mnist.load_data()

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

# ( i allready did )  reshape the data
train_images = train_images / 255.0   
test_images = test_images / 255.0

train_images = train_images.reshape(-1,28*28)    
test_images = test_images.reshape(-1,28*28)

#create a model with 3 layer and  input_shape=(784, )
#compile the model w/  optimizer=adam  , loss = sparse_categorical_crossentropy , metrics =accuracy

model = keras.models.Sequential()
model.add(keras.layers.Dense(units = 128, activation = 'relu' , input_shape =(784, )) )  
model.add(keras.layers.Dropout(0.2))                         
model.add(keras.layers.Dense(units = 32 , activation = 'relu'))
model.add(keras.layers.Dropout(0.4))
model.add(keras.layers.Dense(units = 10 , activation = 'sigmoid') )                               # output layer

model.compile(optimizer= 'adam' , loss = 'sparse_categorical_crossentropy'  , metrics = ['accuracy'])



# ( i allready did )  model summary print
print(model.summary())
#fit the train_images and train_labels with 10 epochs
model.fit(train_images , train_labels , epochs = 5)

#evalute the test_images and test_labes with 1 vebose   AND PRINT ACCURASY

print()
print()
loss , acc   =  model.evaluate(test_images , test_labels , verbose= 1)
print()
print(acc)

#prediction
predictions = model.predict(test_images)    

print(predictions[0])
print(np.argmax(predictions[0]))


